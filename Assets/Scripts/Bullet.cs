﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	GameManager gameManager;
	bool isExplosion;

	void Awake(){
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
	}

	void Start () {
		isExplosion = false;
		gameManager.Explosion.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision collision){
		if(collision.gameObject.CompareTag("Brick") && !isExplosion){
			gameManager.Explosion.SetActive (true);
			gameManager.Explosion.transform.position = transform.position;
			isExplosion = true;
			Destroy (gameObject, 0.5f);
		}
	}
}
