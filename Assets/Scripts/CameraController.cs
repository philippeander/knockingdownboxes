﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class CameraController : MonoBehaviour {

	[SerializeField]float speed = 10.0f;
	[SerializeField]float bulletForce = 1000f;


	GameManager gameManager;
	Transform spawnPoint;
	GameObject bullet;


	void Awake(){
		bullet = Resources.Load ("Bullet") as GameObject;
		spawnPoint = transform.Find ("SpawnPoint");
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 move = new Vector3 (Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0.0f);

		transform.Translate (move * speed * Time.deltaTime);

		if(gameManager.NumOfBullets > 0){
			if(Input.GetKeyUp(KeyCode.Mouse0) && gameManager.ReleaseClick){
				GameObject body = (GameObject) Instantiate (bullet, spawnPoint.position, Quaternion.identity);
				body.GetComponent<Rigidbody>().AddForce (Vector3.forward * bulletForce);
				gameManager.NumOfBullets--;
				gameManager.ReleaseClick = false;
			}
		}
	}
}
