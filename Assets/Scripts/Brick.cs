﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {

	[SerializeField]int pointValue = 12;
	bool isGrounded;

	GameManager gameManager;

	void Awake(){
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	}

	// Use this for initialization
	void Start () {
		isGrounded = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision collision){
		if(collision.gameObject.CompareTag("Ground") && !isGrounded){
			GetComponent<AudioSource> ().Play ();
			gameManager.Points += pointValue;
			gameManager.NumOfKnockedDownBoxes++; 
			isGrounded = true;
		}
	}
}
