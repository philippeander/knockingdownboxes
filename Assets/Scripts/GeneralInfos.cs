﻿using UnityEngine;
using System.Collections;

public class GeneralInfos : MonoBehaviour 
{

	public static int pointsLevel_1 = 0;
	public static int pointsLevel_2 = 0;
	public static int pointsLevel_3 = 0;
	public static int pointsLevel_4 = 0;
	public static int pointsLevel_5 = 0;

	public static int generalPoints = 0;

	public static int GeneralPoints{get{return generalPoints;}set{generalPoints = value;}}

	public static void SetPoint(int id, int m_value){
		
		switch(id){
		case 1:
			pointsLevel_1 = m_value;
			break;
		case 2:
			pointsLevel_2 = m_value;
			break;
		case 3:
			pointsLevel_3 = m_value;
			break;
		case 4:
			pointsLevel_4 = m_value;
			break;
		case 5:
			pointsLevel_5 = m_value;
			break;
		}

		generalPoints = pointsLevel_1 +
						pointsLevel_2 +
						pointsLevel_3 +
						pointsLevel_4 +
						pointsLevel_5;

		print (generalPoints);
	} 

	public static void InitPontuation(){
		pointsLevel_1 = 0;
		pointsLevel_2 = 0;
		pointsLevel_3 = 0;
		pointsLevel_4 = 0;
		pointsLevel_5 = 0;

		generalPoints = 0;
	}

}
