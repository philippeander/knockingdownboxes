﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	[Header("Configurar Manualmente")]
	[SerializeField]int minimumLevelScore = 0;
	[SerializeField]int numOfBulletsOnLevel = 3;
	[SerializeField]int numOfBricksOnLevel = 0;


	int numOfKnockedDownBricks = 0;
	GameObject explosion;
	GameObject pnlYouLose;
	GameObject pnlYouWin;
	GameObject pnlGameOver;
	Text txtPoints;
	Text txtNumBullets;
	Text txtCountDown;
	Text txtGeneralPoints;
	Text txtFinishScore;
	Text txtLevel;
	bool releaseClick;
	float countDown = 3;
	int points;
	int currentScene;
	int numOfScenes;

	public int Points{get{return points;}set{points = value;}}
	public int NumOfKnockedDownBoxes{get{return numOfKnockedDownBricks;}set{numOfKnockedDownBricks = value;}}
	public int NumOfBullets{get{return numOfBulletsOnLevel;}set{numOfBulletsOnLevel = value;}}
	public GameObject Explosion{get{return explosion;}set{explosion = value;}}
	public bool ReleaseClick{get{return releaseClick;}set{releaseClick = value;}}

	void Awake(){
		txtPoints = GameObject.Find ("txtPoints").GetComponent<Text> ();
		txtNumBullets = GameObject.Find ("txtBulletsNum").GetComponent<Text>();
		txtCountDown = GameObject.Find ("txtCountDown").GetComponent<Text> ();
		txtGeneralPoints = GameObject.Find ("txtGeneralPontuation").GetComponent<Text> ();
		txtFinishScore = GameObject.Find ("txt_LestScore").GetComponent<Text> ();
		txtLevel = GameObject.Find ("txtLevel").GetComponent<Text> ();
		pnlGameOver = GameObject.Find ("Pnl_GameOver");
		explosion = GameObject.Find ("ExplosionMobile");
		pnlYouLose = GameObject.Find ("Pnl_YouLose");
		pnlYouWin = GameObject.Find ("Pnl_YouWin");
	}

	// Use this for initialization
	void Start ()
	{
		explosion.SetActive (false);
		pnlYouWin.SetActive (false);
		pnlYouLose.SetActive (false);
		pnlGameOver.SetActive (false);

		currentScene = SceneManager.GetActiveScene().buildIndex;
		numOfScenes = SceneManager.sceneCountInBuildSettings;
		releaseClick = false;
		txtCountDown.text = "";

		if(currentScene == 0){
			GeneralInfos.InitPontuation ();
		}

		txtGeneralPoints.text = "Genaral Points: "+ GeneralInfos.generalPoints.ToString ();
		txtLevel.text = "Level " + (1 + currentScene).ToString()+" / "+ numOfScenes.ToString();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		txtNumBullets.text = numOfBulletsOnLevel.ToString () + " Bullets";
		txtPoints.text = points.ToString () + " / " + minimumLevelScore.ToString ();

		GameConditionsToEndLevel ();
		CountDownToReleaseClick ();
	}

	void IdentifyLevelToSetGlobal(){
		GeneralInfos.SetPoint (currentScene + 1, points);
	} 

	void CountDownToReleaseClick(){
		if(!releaseClick && numOfBulletsOnLevel > 0){
			txtCountDown.text = countDown.ToString ("0");
			countDown -= Time.deltaTime;
			if(countDown <= 0){
				releaseClick = true;
				countDown = 3;
				txtCountDown.text = "";
			}
		}
	}

	void GameConditionsToEndLevel(){
		//Se o numero de caixas do level for igual ao numero de caixas derrubadas
		if(numOfBricksOnLevel == numOfKnockedDownBricks){
			StartCoroutine (DelayNextScene());//Ele passa para o proximo level
		}

		//Se as balas acabarem
		if(numOfBulletsOnLevel <= 0){
			StartCoroutine (DelayToDecision());
		}
			
	}



	IEnumerator DelayToDecision(){
		yield return new WaitForSeconds (5.0f);
		//Se o numero de pontos for maior que o minimo exigido
		if(points >= minimumLevelScore){
			StartCoroutine (DelayNextScene()); //Ele passa para o proximo level
		}
		//Se o numero de pontos for menor que o minimo exigido
		if(points < minimumLevelScore){
			StartCoroutine (YouLoseAction());//Ele não passa para o proximo level

		}
	}


	IEnumerator DelayNextScene(){
		IdentifyLevelToSetGlobal ();

		if (currentScene + 1 == numOfScenes) {
			pnlGameOver.SetActive (true);
			txtFinishScore.text = GeneralInfos.GeneralPoints.ToString();
			if(Input.GetKeyUp(KeyCode.Space)){
				//GeneralInfos.InitPontuation ();
				yield return new WaitForSeconds (0.5f);
				SceneManager.LoadScene (0);
			}
		}
		if (currentScene + 1 < numOfScenes) {
			pnlYouWin.SetActive (true);
			yield return new WaitForSeconds (5.0f);
			SceneManager.LoadScene (currentScene + 1);
		}
	}

	IEnumerator YouLoseAction(){
		pnlYouLose.SetActive (true);
		IdentifyLevelToSetGlobal ();
		yield return new WaitForSeconds (5.0f);
		SceneManager.LoadScene (currentScene);
	}




}
